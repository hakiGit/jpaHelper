package cn.cucc.utils;

import java.io.BufferedReader;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.cucc.bean.PageReq;
import cn.cucc.bean.PageResp;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.ZipUtil;
import cn.hutool.json.JSONUtil;

/**
 * 数据库导入导出工具
 *
 */
@Service
public class ImportOrExportUtil {
	// 写链接(写到主库,可使用事务)
	@Autowired
	JpaHelper jpaHelper;
	@Autowired
	PackageUtil packageUtil;

	public void exportDb(String path) {
		path = path.replace(".zip", "");
		FileUtil.del(path);
		FileUtil.del(path + ".zip");
		try {
			Set<Class<?>> set = ClassUtil.scanPackage(packageUtil.getMainPackage());
			PageReq pageReq = new PageReq();
			pageReq.setPageSize(1000);

			for (Class<?> clazz : set) {
				Entity table = clazz.getAnnotation(Entity.class);
				if (table != null) {
					pageReq.setPageNum(1);
					while (true) {
						PageResp pageResp = jpaHelper.findPageByQuery(pageReq, clazz);
						if (pageResp.getList().size() == 0) {
							break;
						}

						List<String> lines = new ArrayList<String>();
						for (Object object : pageResp.getList()) {
							lines.add(JSONUtil.toJsonStr(object));
						}
						FileUtil.appendLines(lines, path + File.separator + clazz.getSimpleName() + ".json", "UTF-8");
						System.out.println(clazz.getSimpleName() + "表导出了" + pageResp.getList().size() + "条数据");
						pageReq.setPageNum(pageReq.getPageNum() + 1);
					}
				}
			}
			ZipUtil.zip(path);

		} catch (Exception e) {
			e.printStackTrace();
			FileUtil.del(path + ".zip");
		}

		FileUtil.del(path);
	}

	public void importDb(String path) {
		if (!FileUtil.exist(path)) {
			System.out.println(path + "文件不存在");
			return;
		}
		BufferedReader reader = null;

		path = path.replace(".zip", "");
		FileUtil.del(path);
		ZipUtil.unzip(path + ".zip");
		try {
			Set<Class<?>> set = ClassUtil.scanPackage(packageUtil.getMainPackage());
			for (Class<?> clazz : set) {
				Entity table = clazz.getAnnotation(Entity.class);
				if (table != null) {
					File file = new File(path + File.separator + clazz.getSimpleName() + ".json");
					if (file.exists()) {
						jpaHelper.deleteByQuery(new ConditionAndWrapper(), clazz);

						reader = FileUtil.getReader(file, "UTF-8");
						List<Object> list = new ArrayList<Object>();
						while (true) {
							String json = reader.readLine();
							if (StrUtil.isEmpty(json)) {
								insertAll(list);
								System.out.println(clazz.getSimpleName() + "表导入了" + list.size() + "条数据");
								list.clear();
								break;
							}
							list.add(JSONUtil.toBean(json, clazz));
							if (list.size() == 1000) {
								insertAll(list);
								System.out.println(clazz.getSimpleName() + "表导入了" + list.size() + "条数据");
								list.clear();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			IoUtil.close(reader);
		}
		FileUtil.del(path);
	}

	private void insertAll(List<Object> list) {
		for (Object object : list) {
			jpaHelper.insert(object);
		}

	}
}
