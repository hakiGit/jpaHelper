package cn.cucc.utils;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InitUtil {
	@Autowired
	InitTool initTool;
	
	@PostConstruct
	public void init() {
		initTool.init();
	}
}
