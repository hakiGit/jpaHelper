package cn.cucc.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.cucc.bean.Comment;
import cn.cucc.bean.InitValue;
import cn.cucc.bean.Update;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;

@Service
public class InitTool {
	@Autowired
	PackageUtil packageUtil;

	@Autowired
	JpaHelper jpaHelper;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Value("${spring.datasource.url:}")
	String url;

	@Transactional
	public void init() {
		// 初始化数据库注释
		Set<Class<?>> set = ClassUtil.scanPackage(packageUtil.getMainPackage());
		List<String> sqls = new ArrayList<>();
		for (Class<?> clazz : set) {
			Entity entity = clazz.getAnnotation(Entity.class);
			if (entity != null) {
				// 更新数据库注释
				Comment comment = clazz.getAnnotation(Comment.class);
				if (comment != null && StrUtil.isNotEmpty(comment.value())) {
					sqls.add(createTableComment(clazz, comment.value()));
				}

				Field[] fields = ReflectUtil.getFields(clazz);
				for (Field field : fields) {
					comment = field.getAnnotation(Comment.class);
					if (field.isAnnotationPresent(Comment.class) && StrUtil.isNotEmpty(comment.value())) {
						String sql = createColumComment(clazz, field, comment.value());
						if(StrUtil.isNotEmpty(sql)) {
							sqls.add(sql);
						}
					}
				}
			}
		}

		if (sqls.size() > 0) {
			jdbcTemplate.batchUpdate(sqls.toArray(new String[sqls.size()]));
		}

		// 注入默认值
		for (Class<?> clazz : set) {
			Entity entity = clazz.getAnnotation(Entity.class);
			if (entity != null) {
				Field[] fields = ReflectUtil.getFields(clazz);
				for (Field field : fields) {
					if (field.isAnnotationPresent(InitValue.class)) {
						InitValue defaultValue = field.getAnnotation(InitValue.class);
						if (defaultValue.value() != null) {
							// 更新默认值
							Long count = jpaHelper.findCountByQuery(new ConditionAndWrapper().isNull(field.getName()), clazz);
							if (count > 0) {
								String value = defaultValue.value();
								Object obj = null;
								// 获取字段类型
								Class<?> type = field.getType();
								if (type.equals(String.class)) {
									obj = value;
								}
								if (type.equals(Short.class)) {
									obj = Short.parseShort(value);
								}
								if (type.equals(Integer.class)) {
									obj = Integer.parseInt(value);
								}
								if (type.equals(Long.class)) {
									obj = Long.parseLong(value);
								}
								if (type.equals(Float.class)) {
									obj = Float.parseFloat(value);
								}
								if (type.equals(Double.class)) {
									obj = Double.parseDouble(value);
								}
								if (type.equals(Boolean.class)) {
									obj = Boolean.parseBoolean(value);
								}

								jpaHelper.updateByQuery(new ConditionAndWrapper().isNull(field.getName()), new Update().set(field.getName(), obj), clazz);
							}
						}
					}
				}
			}
		}
	}

	private String createColumComment(Class<?> clazz, Field field, String value) {
		String tableName = StrUtil.toUnderlineCase(clazz.getSimpleName());
		String columName = StrUtil.toUnderlineCase(field.getName());

		if (url.contains("mysql") || url.contains("sqlite")) {
//			return "UPDATE information_schema.COLUMNS t  SET t.column_comment  = '" + value + "' WHERE t.TABLE_SCHEMA= '" + database + "' AND t.table_name='" + tableName + "' AND t.COLUMN_NAME= '"
//					+ columName + "'";
			
			return null;
		} else {
			return "COMMENT ON COLUMN \"" + tableName + "\".\"" + columName + "\" IS '" + value + "'";
		}
	}

	private String createTableComment(Class<?> clazz, String value) {
		String tableName = StrUtil.toUnderlineCase(clazz.getSimpleName());

		if (url.contains("mysql") || url.contains("sqlite")) {
			return "ALTER TABLE `" + tableName + "` comment '" + value + "'";
		} else {
			return "COMMENT ON TABLE \"" + tableName + "\" IS '" + value + "'";
		}
	}


}
