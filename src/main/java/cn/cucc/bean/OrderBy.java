package cn.cucc.bean;

public class OrderBy {
	Direction direction;
	String column;

	public static enum Direction {
		ASC, DESC;
	}

	public OrderBy(Direction direction, String column) {
		this.direction = direction;
		this.column = column;
	}
	
	public OrderBy() {
		
	}
	

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

}
