package cn.cucc.bean;

import io.swagger.annotations.ApiModelProperty;

/**
 * 分页请求类
 * 
 */
public class PageReq {
	@ApiModelProperty("起始页,从1开始")
	Integer pageNum = 1;
	@ApiModelProperty("每页记录数,默认为10")
	Integer pageSize = 10;
	public Integer getPageNum() {
		return pageNum;
	}
	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}


}
