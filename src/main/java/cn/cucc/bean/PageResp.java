package cn.cucc.bean;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * 分页返回类
 * 
 */
public class PageResp<T> {
	@ApiModelProperty("总记录数")
	Long total = 0l;
	@ApiModelProperty("起始页,从1开始")
	Integer pageNum = 1;
	@ApiModelProperty("每页记录数,默认为10")
	Integer pageSize = 10;
	
	@ApiModelProperty("起始编号")
	Integer startRow;
	@ApiModelProperty("结束编号")
	Integer endRow;
	@ApiModelProperty("总页数")
	Integer pages;
	
	@ApiModelProperty("内容列表")
	List<T> list;

	/**
	 * 清空List
	 */
	public void clearList() {
		list = new ArrayList<>(); 
	}
	
	
	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public Integer getStartRow() {
		return startRow;
	}

	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	public Integer getEndRow() {
		return endRow;
	}

	public void setEndRow(Integer endRow) {
		this.endRow = endRow;
	}

	public Integer getPages() {
		return pages;
	}

	public void setPages(Integer pages) {
		this.pages = pages;
	}


}
