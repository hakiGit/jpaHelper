package cn.cucc.bean;

import java.util.HashMap;
import java.util.Map;

import cn.cucc.reflection.ReflectionUtil;
import cn.cucc.reflection.SerializableFunction;

public class Update {

	Map<String, Object> sets;

	public Update() {
		sets = new HashMap<String, Object>();
	}

	public Update set(String key, Object value) {
		sets.put(key, value);
		return this;
	}

	public <T, R> Update set(SerializableFunction<T, R> property, Object value) {
		sets.put(ReflectionUtil.getFieldName(property), value);
		return this;
	}

	public Map<String, Object> getSets() {
		return sets;
	}

	public void setSets(Map<String, Object> sets) {
		this.sets = sets;
	}

}
